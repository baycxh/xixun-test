[TOC]

# WebSocket开发方案

## 1、获取基础信息和心跳

需要接收控制卡设备基础信息和心跳包需要实现一个websocket server，把服务地址通过LedOK设置到控制卡服务器地址中。例如你的websocket服务部署在192.168.8.99：8888，那么在LedOK中如下图设置：

![image-20200227164158738](../pictures/websocket-ledok.png)

当控制卡连上你们的服务时会先后发送3条数据：

1、控制卡序列号（字符串数据，例：y10-618-01234）

2、加密信息二进制数据，客户可以忽略这条数据

3、认证信息二进制数据，客户可以忽略这条数据

服务接收完这3条数据，需要向控制卡（client）发送JSON字符串

{"_type":"GetCardInfo","id":"first"}

client接收后会返回JSON字符串包含控制卡所有基础信息

4、当服务器收到控制卡发来空数据，建议答复一个空数据，维持心跳，可以掌握设备是否在线。

 

 

GetCardInfo返回信息格式举例：

{

  "_type":"CardInfo",

  "cardId":"y10-817-01926", //设备id

  "commandId":"first",

  "card":{

​    "alias":"noname",	//设备别名

​    "updateVersion":"6.6",	//update程序版本号

​    "sim":{					//SIM卡相关信息

​      "deviceId":null,

​      "networkCountryIso":"",

​      "number":null,

​      "simCountryIso":"",

​      "simOperatorName":"",

​      "simSerialNumber":null,

​      "subscriberId":null,

​      "simState":0

​    },

​    "screenStatus":"on",		//屏幕亮灭状态：on开屏，off关屏

​    "companyId":"test",		//公司ID，可忽略

​    "connVersion":"9.8.4",	//conn程序版本

​    "playerVersion":"10.2.8",	//xixunplayer程序版本

​    "currentProgramId":null,	//当前播放节目的id

​    "currentProgramName":null, //当前播放节目名

​    "netType":"ETH",	//网络连接方式：ETH有线，WLAN无线，其他3/4G

​    "ledsetVersion":"5.0.3.4", //cardsystem程序版本

​    "humidity":0,	//当前湿度

​    "lat":0,	//当前GPS纬度

​    "diskSize":0, //未使用

​    "lng":0,//当前GPS经度

​    "ledsetVersionCode":459, 

​    "locked":false, //

​    "height":512, //大屏高度

​    "connVersionCode":80, 

​    "playerVersionCode":328,

​    "rssi":-9999, 

​    "brightness":64, //屏幕亮度

​    "autoBrightness":false, //是否根据光感自动调节屏幕亮度

​    "temperature":0, //当前设备传感器返回的温度

​    "asu":99,

​    "updateVersionCode":15,

​    "volume":12, //设备音量

​    "width":1280 //大屏宽度

  }

} 

