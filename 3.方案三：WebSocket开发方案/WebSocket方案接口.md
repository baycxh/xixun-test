[TOC]

# Websocket方案接口

## 1.返回失败的数据格式

以下接口发送接收的数据都是Json字符串;

```json
// 请求失败返回的数据
{
  "_type":"Error",
  "cardId":"y10-116-01576",
  "commandId":null,
  "errorMessage":"CommandType Do Not Exist"
}
```



## 2.屏幕截图

### 1.屏幕截图

发送数据

```json
{
"_type": "GetScreenshot",
  "id":"5ec4d70d801daf892447adf0"
}
```

返回数据

```json
// 请求成功
{
 "_type": "Screenshot",
 "cardId": "y10-116-01576",
 "commandId": "5ec4d70d801daf892447adf0",
 "screenshot":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABsSFBcUERsXFhceHBsgKEIrKCUlKFE6PTBCYFVlZF9VXVtqeJmBanGQc1tdhbWGk
J6jq62rZ4C8ybqmx5moq6T/2wBDARweHigjKE4rK06kbl1upKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpK
T/wAARCABmAQADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUE
GE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJ
ipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAA
AAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJy
gpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMn
K0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDmaKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii
igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiii
gAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiig
AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigA
ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo
oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/Z",
//返回的截图地址⻚⾯展⽰’data:image/jpeg;base64‘+data.screenshot,⽆截图时返回null
}
```



### 2.摄像头截图

发送数据

```json
{
  "_type":"GetScreenshotByCamera",
  "sendTo":"joey-cardsystem"
}
```

返回数据

```json
// 成功
{
 "_type": "DataCallback",
 "cardId": "y10-116-01576",
 "commandId": "",
 "screenshot": null,//返回的截图地址⻚⾯⽰’data:image/jpeg;base64‘+data.screenshot,⽆截图时返回null
}
```



### 3.安卓全尺⼨截图

发送数据

```json
{
  "_type": "ScreenshotBySize",
  "width": -1,
  "height": -1,
  "quality": 100,
  "scale": 100,
  "sendTo": "joey-cardsystem",
  "id": "5ec4cf666dc7f1a3156097ed"
}
```

返回数据

```json
// 请求成功
{
 "_type": "DataCallback",
 "image": null,//返回的截图地址⻚⾯展⽰’data:image/jpeg;base64‘+data.image,⽆截图时返回null
 "commandId": "5ebcef85fff26f9f2e9f0cd6",
 "cardId": "e30-b19-40033",
}
```



## 3.屏幕开关

### 1.手动设置屏幕开关

发送数据

```json
{
"_type": "SwitchScreen",
  "turnOn":true,//boolean; true 开屏 false关屏
  "id":"5ec4d7b8f44de6ae5e2b56f0"
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId": "5ec4d7b8f44de6ae5e2b56f0",
 "_cardId": "e30-b19-40033"
}
```



### 2.查询屏幕定时开关情况

发送数据

```json
{
"_type": "GetTaskToKeepScreenOn",
"id":"5ec4d7b8f44de6ae5e2b56f0"
}
```

返回数据

```json
// 请求成功
{"_type": "DataCallback",
"cardId": "e30-b19-40033",
 "commandId":"5ec4d7b8f44de6ae5e2b56f0"
 "task": {
  "createBy": "check",
  "createDate": "2020-04-23T08:06:43.448Z",
  "name": "0423",// 定时任务名
  "schedules": [{
    "dateType": "Range",// Range代表指定⽇期，All代表⽆限制
    "endDate": "2020-04-30",//结束⽇期
    "endTime": "17:30",//结束时间
    "filterType": "Week",// None代表没有星期限制，Week代表有星期限制
    "monthFilter": [],
    "startDate": "2020-04-23",//开始⽇期
    "startTime": "08:30",//开始时间
    "timeType": "Range",// Range代表指定时间，All代表⽆限制
    "weekFilter": [1,2,3,4,5]//0`6，0代表星期⽇ 6代表星期六
  }]
},// ⽆定时任务时为空
```



### 3.发送定时开关屏任务

发送数据

```json
{
  "cardId": "e30-b19-40033",
  "_type": "TaskKeepingScreenOn",
  "id": "5ea14c930e331f366eafffa2",
  "task": {
    "createBy": "check",
    "createDate": "2020-04-23T08:06:43.448Z",
    "dateCreated": "2020-04-23T08:06:43.448Z",
    "id": "5ea14c930e331f366eafffa2",
    "lng": "zh-CN",
    "name": "0423",
"createBy": "check",
    "createDate": "2020-04-23T08:06:43.448Z",
    "dateCreated": "2020-04-23T08:06:43.448Z",
    "id": "5ea14c930e331f366eafffa2",
    "lng": "zh-CN",
    "name": "0423",
    "schedules": [{
      "dateType": "Range",// Range代表指定⽇期，All代表⽆限制
      "endDate": "2020-04-30",//结束⽇期
      "endTime": "17:30",//结束时间
      "filterType": "Week",// None代表没有星期限制，Week代表有星期限制
      "monthFilter": [],
      "startDate": "2020-04-23",//开始⽇期
      "startTime": "08:30",//开始时间
      "timeType": "Range",// Range代表指定时间，All代表⽆限制
      "weekFilter": [1,2,3,4,5]//0`6，0代表星期⽇ 6代表星期六
  }]
 },
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId": "5ea14c930e331f366eafffa2",
}
```



## 4.屏幕亮度

### 1.设置灵敏度和最小亮度

发送数据

```json
{
  "minBrightness": 1,// 最小亮度
  "sensitivity": 50,//灵敏度0到100
  "_type": "SetAutoBrightness"
  "sendTo":"joey-cardsystem",
  "id":"5ec4d1d1801daf892447ade7"
}
```

返回数据

```json
// 请求成功
{
 "cardId": "e30-b19-40033",
 "_type": "Success",
 "commandId": "5ebcf586fff26f9f2e9f7b0e",
 "_cardId": "y10-116-01576"
}
```



### 2.查询灵敏度和最小亮度

发送数据

```json
{
  "_type":"GetAutoBrightness",
  "sendTo":"joey-cardsystem",
  "id":"5ec4d236801daf892447ade8"
}
```

返回数据

```json
// 请求成功
{
  "_type":"DataCallback",
  "sensitivity":50,
  "minBrightness":1,
  "commandId":"5ec4d236801daf892447ade8",
  "cardId":"e30-b19-40034"
}
```



### 3.手动设置屏幕亮度

发送数据

```json
{
  "brightness": 8,
  "_type": "SetBrightness",
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId": "5ebcf724fff26f9f2e9f9f84",
}
```



### 4.发送定时亮度任务

发送数据

```json
{
  "_type": "TaskSettingBrightness",
  "id":"5ec4d1d1801daf892447ade7",
"task": {
"createBy": "lucky2",
    "createDate": "2019-11-14T01:21:50.907Z",
    "dateCreated": "2019-11-14T01:21:50.907Z",
    "defaultBrightness": 5,// 默认亮度
    "items": [{
      "brightness": 58,// 该限制条件下的亮度
      "schedules": [{
        "dateType": "All",// Range代表指定⽇期，All代表⽆限制
        "endDate": null,
        "endTime": "09:50",
        "filterType": "None",// None代表没有星期限制，Week代表有星期限制
        "lng": "zh-CN",
        "monthFilter": [],
        "startDate": null,
        "startTime": "09:00",
        "timeType": "Range",// Range代表指定时间，All代表⽆限制
        "weekFilter": [],
     }]
   }],
 }
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId": "5ec4d1d1801daf892447ade7",
}
```



### 5.查询屏幕定时亮度

发送数据

```json
{
  "_type": "GetTaskToSetBrightness",
  "id":"5ec4d1d1801daf892447ade7",
}
```

返回数据

```json
// 请求成功
{"_type":"DataCallback",
 "cardId":"e30-b19-40033",
 "commandId":"5ec4d1d1801daf892447ade7",
 "task":{ // 为null时代表⽆定时任务
   "createBy":"lucky2",
    "createDate":"2019-11-14T01:21:50.907Z",
    "defaultBrightness":5,
    "items":[{
        "brightness":58,
        "schedules":[{
         "dateType":"All",
              "endDate":null,
              "endTime":"09:50",
              "filterType":"None",
              "monthFilter":[],
              "startDate":null,
              "startTime":"09:00",
              "timeType":"Range",
              "weekFilter":[]
       }]},
        {"brightness":10,
         "schedules":[{
           "dateType":"All",
           "endDate":null,
           "endTime":"09:55",
           "filterType":"None",
           "monthFilter":[],
           "startDate":null,
           "startTime":"09:00",
           "timeType":"Range",
           "weekFilter":[]}]}],
 "name":"576",
 "brightness":5
 }
}
```



## 5.音量控制

### 1.手动设置音量

发送数据

```json
{
  "id":"5ec4d1d1801daf892447ade7"
  "volume": 10,// 范围0到15
  "_type": "SetVolume",
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId": "5ec4d1d1801daf892447ade7",
}
```



### 2.发送定时音量任务

发送数据

```json
{
"id":"5ec4d1d1801daf892447ade7"
  "_type": "TaskSettingVolume",
"task": {
"createBy": "lucky2",
    "createDate": "2019-11-14T01:21:50.907Z",
    "dateCreated": "2019-11-14T01:21:50.907Z",
    "defaultVolume": 5,// 默认⾳量
    "id": "5dccac2eee30d0611c2baace",
    "items": [{
      "volume": 8,// 该限制条件下的⾳量
      "id": "5dccac2eee30d0611c2baad0",
      "schedules": [{
        "dateType": "All",
        "endDate": null,
        "endTime": "09:50",
        "filterType": "None",
        "lng": "zh-CN",
        "monthFilter": [],
        "startDate": null,
        "startTime": "09:00",
        "timeType": "Range",
        "weekFilter": [],
     }]
   }],
 }
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId":"5ec4d1d1801daf892447ade7",
}
```

### 2.查询定时音量任务

发送数据

```json
{
 "id":"5ec4d1d1801daf892447ade7",
  "_type": "GetTaskToSetVolume",
}
```

返回数据

```json
// 请求成功
{
 "_type": "DataCallback",
 "cardId": "e30-b19-40033",
 "commandId": "5ebcf8ccfff26f9f2e9fbdc0",
 "task": null,// ⽆任务时为null，有定时任务时参照发送定时任务的task格式
 "_cardId": "y10-116-01576"
}
```



## 6.重启系统

### 1.重启系统

发送数据

```json
{
  "id":"5ec4d1d1801daf892447ade7",
  "_type": "Reboot",
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId":"5ec4d1d1801daf892447ade7",
}
```



### 2.定时重启

发送数据

```json
{
  "_type":"SetScheduleToReboot",
  "time":"00:00",
  "sendTo":"joey-cardsystem",
  "id":"5ec4d3f6801daf892447adea"
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId":"5ec4d3f6801daf892447adea"
}
```



### 3.查询定时重启

发送数据

```json
{
  "_type":"GetScheduleToReboot",
  "sendTo":"joey-cardsystem",
  "id":"5ec4d3b2801daf892447ade9"
}
```

返回数据

```json
// 请求成功
{
 "_type": "DataCallback",
 "time": "03:00",// 重启时间点
 "commandId": "5ec4d3b2801daf892447ade9"
 "cardId": "e30-b19-40033",
}
```



## 7.参数配置

### 1.设置终端别名、gps坐标回报间隔、NTP服务器地址、时区

发送数据

```json
{
"cardAlias": "e30-30",// 终端别名
  "id":"5ec4d1d1801daf892447ade7",
  "locationFeedback": 0,//gps坐标回报间隔 单位秒
  "ntpServer": "",//NTP服务器地址
  "timezone": "",//时区
  "usbProgramPwd": "",//U盘节⽬密码
  "_type": "SetConnectionConfig"
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId": "5ec4d1d1801daf892447ade7",
}
```



### 2.查询终端别名、gps坐标回报间隔、NTP服务器地址、时区

发送数据

```json
{
  "id":"5ec4d1d1801daf892447ade7",
  "_type": "GetNtpAndTimezone",
}
```

返回数据

```json
// 请求成功
{
 "_type": "DataCallback",
 "cardId": "e30-b19-40033",
 "commandId": "5ec4d1d1801daf892447ade7",
 "language": "en",//语⾔
 "locationFeedback": 0,//GPS坐标回报间隔(秒)
 "locked": false,//锁定 锁定控制卡后，控制卡将不能更换服务器地址和执⾏与显⽰相关的操作
 "now": "May 14, 2020 5:07:10 PM",//当前时间
 "ntpServer": "ntp1.aliyun.com",//NTP服务器地址
 "timezone": "Asia/Shanghai",//时区
}
```



### 3.查询GPS

发送数据

```json
{
 "id":"5ec4d1d1801daf892447ade7",
  "_type": "GetGpsInfo",
}
```

返回数据

```json
// 请求成功
{
 "_type": "DataCallback",
 "cardId": "e30-b19-40033",
 "commandId": null,
 "lat": 0.0,//纬度
 "lng": 0.0,//经度
 "satelliteNumber": 0,//卫星数⽬
 "speed": 0.0,//速度 km/h
}
```



### 4.同步设置

发送数据

```json
{
  "id":"5ec4d1d1801daf892447ade7",
  "_type": "SetSync",
  "checkNtpTime": 10,//同步间隔（分钟/次）仅在NTP模式下⽣效
  "brightness": "none",//亮度 serial代表⽅式1,none代表不做同步 仅在Lora模式下⽣效
  "delaySync": "1000",//时间偏移量（毫秒）仅在Lora模式下⽣效
  "identificationCode": "xixun",//同步识别码 仅在Lora模式下⽣效
  "screenSwitch": "none",//屏幕开关 serial代表⽅式1,none代表不做同步 仅在Lora模式下⽣效
  "time": "serial",//serial代表LOra,gps代表GPS,ntp代表NTP 仅在Lora模式下⽣效
  "volume": "none",//⾳量 serial代表⽅式1,none代表不做同步 仅在Lora模式下⽣效
  "sendTo":"joey-cardsystem",
  "id":"5ec4d4cb801daf892447adeb"
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId": "5ec4d4cb801daf892447adeb"
}
```



### 5.查询同步设置

发送数据

```json
{
 "_type": "GetSync",
 "sendTo":"joey-cardsystem",
 "id":"5ec4d5f7801daf892447adec"
}
```

返回数据

```json
// 请求成功
{
 "_type": "DataCallback",
 "cardId": "e30-b19-40033",
 "commandId": "5ec4d5f7801daf892447adec",
 "brightness": "NONE",
 "checkNtpTime": 10,
 "delaySync": 1000,
 "identificationCode": "xixun",
 "lastSynchronousTime": 1589418195009,
 "screenSwitch": "NONE",
 "time": "SERIAL",
 "volume": "NONE",
}
```



### 6.设置服务器地址、公司id 、realtime Sever

发送数据

```json
{
  "id":"5ec4d1d1801daf892447ade7",
  "companyId": "alahover",//公司id
  "realtimeURL": "192.168.8.48:8888",// realtime Sever
  "serverURL": "192.168.8.48:8089",//服务器地址
  "_type": "SetAdvancedConfig",
}
```

返回数据

```json
// 请求成功
{
 "_type": "Success",
 "cardId": "e30-b19-40033",
 "commandId":"5ec4d1d1801daf892447ade7",
}
```



### 7.硬件状态

发送数据

```json
{
  "_type":"GetFpgaInfo",
  "id":"5ec4e9448ed2644265dad9a2"
}
```

返回数据

```json
{"_type":"FpgaInfoCallback",
 "cardId":"e30-b19-40034",
 "commandId":"5ec4e9448ed2644265dad9a2",
 "list":[{
   "cardVoltage":"4.86V",// 卡电压
   "doorOpened":"Open", // ⻔是否被打开
   "externalVoltage1":"0.0V",// 外部电压1
   "externalVoltage2":"0.0V",// 外部电压2
   "height":"0",
   "humidity":"0.0%",// 湿度
   "smoke":"Normal",// 有烟雾
   "temperature":"255.0℃", // 温度
   "version":"7029",// 硬件版本
   "width":"0",
   "x":"0",
   "y":"0"
    }]
}
```



### 8.设置警报

发送数据

```json
{
  "_type":"SetWarning",
  "smoke":true,// 是否有烟雾
  "doorOpened":true,// ⻔是否被打开
  "externalVoltage2":{ // 外部电压2
    "ceiling":5,// 上限
    "floor":0// 下限
 },
  "externalVoltage1":{// 外部电压1
    "ceiling":5,// 上限
    "floor":0// 下限
 },
  "cardVoltage":{//卡电压
    "ceiling":5,// 上限
    "floor":0// 下限
 },
  "humidity":{//湿度
 "ceiling":70,// 上限
    "floor":0// 下限
 },
  "temperature":{//温度
 "ceiling":60,// 上限
    "floor":0// 下限
 },
 "id":"5ec4eb7e8ed2644265dad9a4"
}
```

返回数据

```json
// 成功
{
  "_type":"Success",
 "cardId":"e30-b19-40034",
  "commandId":"5ec4eb7e8ed2644265dad9a4"
}
```



### 9.查询警报

发送数据

```json
{
  "_type":"GetWarning",
  "id":"5ec4ed6e8ed2644265dad9a6"
}
```

返回数据

```json
{
  "_type":"WarningInfoCallback",
  "cardId":"e30-b19-40034",
  "commandId":"5ec4ed6e8ed2644265dad9a6",
  "setting":{
    "cardVoltage":{
      "ceiling":5.0,
      "floor":0.0
   },
    "doorOpened":true,
    "externalVoltage1":{
      "ceiling":5.0,
      "floor":0.0
   },
    "externalVoltage2":{
      "ceiling":5.0,
      "floor":0.0
   },
    "humidity":{
      "ceiling":70.0,
      "floor":0.0
   },
 "smoke":true,
    "temperature":{
      "ceiling":60.0,
      "floor":0.0
   }
 }
}
```

### 10.在线更新apk

发送数据

```json
{
  "_type":"UpdateApp",
  "apkId":"5de49d43bf15592b29418b05",
  "apkName":"Display_v516.2.zip",
  "id":"5ec4ee398ed2644265dad9a7"
}
```

返回数据

```json
// 成功
{
  "_type":"Received",
  "cardId":"e30-b19-40034",
  "commandId":"5ec4ef0d8ed2644265dadda0"
}
```



## 8.查询控制卡信息

发送数据

```json
{
"_type":"GetCardInfo",
"id":"5ec4df0b8ed2644265dad9a1"
}
```

返回数据

```json
{

  "_type":"CardInfo",

  "cardId":"y10-817-01926", //设备id

  "commandId":"first",

  "card":{

    "alias":"noname",	//设备别名

    "updateVersion":"6.6",	//update程序版本号

    "sim":{					//SIM卡相关信息

      "deviceId":null,

      "networkCountryIso":"",

      "number":null,

      "simCountryIso":"",

      "simOperatorName":"",

      "simSerialNumber":null,

      "subscriberId":null,

      "simState":0

    },

    "screenStatus":"on",		//屏幕亮灭状态：on开屏，off关屏

    "companyId":"test",		//公司ID，可忽略

    "connVersion":"9.8.4",	//conn程序版本

    "playerVersion":"10.2.8",	//xixunplayer程序版本

    "currentProgramId":null,	//当前播放节目的id

    "currentProgramName":null, //当前播放节目名

    "netType":"ETH",	//网络连接方式：ETH有线，WLAN无线，其他3/4G

    "ledsetVersion":"5.0.3.4", //cardsystem程序版本

    "humidity":0,	//当前湿度

    "lat":0,	//当前GPS纬度

    "diskSize":0, //未使用

    "lng":0,//当前GPS经度

    "ledsetVersionCode":459, 

    "locked":false, //

    "height":512, //大屏高度

    "connVersionCode":80, 

    "playerVersionCode":328,

    "rssi":-9999, 

    "brightness":64, //屏幕亮度

    "autoBrightness":false, //是否根据光感自动调节屏幕亮度

    "temperature":0, //当前设备传感器返回的温度

    "asu":99,

    "updateVersionCode":15,

    "volume":12, //设备音量

    "width":1280 //大屏宽度

  }

} 

```



## 9.发送节目任务（_type，sendTo字段固定参数，其余参数参考xixunplayer节目说明文档）

发送数据

```json
{
			"preDownloadURL":"http://www.m2mled.net/file/download?id=",
            "_type": "PlayXixunTask",
			"sendTo": "yzd-player", 
            "id": "600a297fa480eb3121a02823",
            "notificationURL":"http://192.168.2.12:8083/getJSON",
            "task": {
                "name": "测试媒体LED屏接叱012003",
                "_id": "6007d65eb4ce829b1fef28f4",
                "insert":true,
                "items": [
                    {
                        "_id": "9146b44e-7fe4-4098-b858-f2499b88d0ac",
                        "_program": {
                            "totalSize": 3617267,
                            "name": "测试媒体-LED屏接史1012003",
                            "width": 192,
                            "layers": [
                                {
                                    "sources": [
                                        {
                                            "entryEffectTimeSpan": 0,
                                            "_type": "Video",
                                            "exitEffect": "None",
                                            "entryEffect": "None",
                                            "top": 0,
                                            "size": 3617267,
                                            "left": 0,
                                            "name": "600a297fa480eb3121a02823.mp4",
                                            "width": 192,
                                            "playTime": 0,
                                            "timeSpan": 79,
                                            "lineHeight": 0,
                                            "id": "600a297fa480eb3121a02823",
                                            "exitEffectTimeSpan": 0,
                                            "height": 288,
                                            "md5": "2e7e39eaa3faa90f5d5b34536722ce49"
                                        }
                                    ],
                                    "repeat": false
                                }
                            ],
                            "_id": "600a297fa480eb3121a02823",
                            "height": 288
                        },
						"repeatTimes": 1,
                       "schedules":[]
                    }
                ]
            }
        }
```

返回数据

```json
// 收到的第⼀条消息 代表已成功接收到指令
{
  "_type":"Received",
  "cardId":"e30-b19-40034",
  "commandId":"5ec5e5cfc85856154e321bb9"
}
// 接着会收到关于素材下载进度的Json字符串
{
  "_type":"TaskProgress",
  "cardId":"e30-b19-40034",
  "commandId":"5ec5e5cfc85856154e321bb9",
  "progress":100,
  "remainingSeconds":0,
  "speed":0,
  "taskItemId":"5ec5e056c85856154e320787"
}
// 当progress为100之后 会收到这条代表完全成功的Json字符串
{
  "_type":"Success",
  "cardId":"e30-b19-40034",
  "commandId":"5ec5e5cfc85856154e321bb9"
}
```

## 10、清除控制卡中的节目和数据

功能说明：调用该接口后会将控制卡中所有节目和下载的节目文件彻底删除，播放器显示默认logo图片。

发送数据

```json
{
	"cardId": "y60-620-40490",
	"_type":"DeleteTask",
	"sendTo":"yzd-player"
}
```

返回数据

```json
{
  	"_type": "DataCallback",
  	"result": "received command",
  	"commandId": "61234811c4c3c90c07326ebf",
  	"cardId": "y60-620-40490",
  	"_cardId": "y60-620-40490"
}
```

## 11、获取设备中的节目列表

功能说明：查询的departmentId请与下发节目时的departmentId保持一致，建议下发时departmentId为空或设置为固定值

发送数据

```json
{
	"departmentId":"539eaaedb6e1232a1566d9c2",
	"sendTo":"yzd-player",
	"id":"61262168c4c3c90c07456707",
	"_type":"GetProgramTask"
}
```

返回数据

```json
{
	"id":"61262168c4c3c90c07456707",
	"_type": "ProgramTaskCallback",
	"task":{}
}
//返回数据中的task请参考下发节目时的数据结构
```

## 12、查询剩余磁盘空间

功能说明：该接口可查询设备上内部存储和外部存储的未使用磁盘空间和总容量。

发送数据

```json
{
	"_type":"getSpareSpace",
	"id":"61263007c4c3c90c07468109"
}
```

返回数据

```json
{
	"_type":"SpaceInfo",
	"cardId":"y30-120-00038",
	"commandId":"61263007c4c3c90c07468109",
	"freeExternal":619970560,
	"freeInternal":511963136,
	"totalExternal":31902400512,
	"totalInternal":1059061760
}
```

说明：

freeExternal：可使用的外部存储空间

freeInternal：可使用的内部存储空间

totalExternal：外部存储总容量

totalInternal：内部存储总容量，以上单位均为字节

### 其它功能可参考方案四局域网接口文档，在json参数中加入以下字段即可使用

```json
	"id": "6151973eaf79bc9b3e2f37e3",
	"sendTo": "joey-cardsystem",
	"cardId": "y60-720-c0171"
```

例:引用接口说明中的获取控制卡序列号

```json
{
    "_id": "98e8d3cd47fad6ce8e3f7b8d42cb4d9b",
	"_type": "GetCardName",
	"sendTo": "joey-cardsystem",
	"id": "6151973eaf79bc9b3e2f37e3",
	"cardId": "y60-720-c0171"
}
```

