# 		xixunPlayer节目制作方案说明

### 若xwalk播放满足不了复杂播放功能，可将节目播放转换为xixunplayer节目格式，使用熙讯默认播放器来播放节目。

### playerDemo演示图片和视频轮播的节目格式，对应的实例协助用户迅速完成节目制作而不需要在前期花大量时间研究我们的节目结构。

### 实例项目使用说明：

### 在运行项目前需要将项目目录下的res文件夹复制到C盘根目录。该文件夹包含节目模板文件和需要播放的图片和视频。