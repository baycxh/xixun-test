通过UDP广播找卡请参考以下流程：

1、创建UDP socket；

2、socket发送广播：

广播地址255.255.255.255，广播端口号22222

广播数据格式为JSON，内容为：

{"action":"getInfo"}

3、socket发完广播后会收到控制卡的应答消息，消息内容：{"cardId":"y10-116-01576",
//序列号

"brightness":255, //亮度

"id":"01C3D4K0YFCFRZPGSZT5VXQHSS", //忽略

"androidVersion":"4.0.3", //安卓系统版本

"starterPort":3000, //starter程序使用的端口号

"height":32, //屏幕高度

"androidWidth":1280, //安卓系统分辨率

"width":64, //屏幕宽度

"name":"橙子太好吃", //控制卡别名

"versionName":"starter164", //starter程序版本号

"androidHeight":476, //安卓分辨率

"version":164} //starter版本代码

其中设备IP地址可通过通讯的socket API获取。
