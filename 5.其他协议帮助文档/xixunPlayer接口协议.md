# 				xixunPlayer接口协议

## 1.播放任务

#### 通过AIDL接口executeJsonCommand传递数据。

**命令数据：**

```json
{ 
    "_type": "PlayXixunTask", 
    "id": "abc", 							   *//Command ID*
    "preDownloadURL":"http://aaaa/bbbb?id=", 	 *//下载地址前缀* 
    "notificationURL":"http://xxxxxx" 			*//下载进度的通知地址*
    "task":{}, 								  *//节目任务* 
}
```

**收到命令后可以做判断空间不足等操作，若有错误返回JSON字符串**

```json
{"_type":"Error","errorMessage":"xxxx"}
```

**无错误返回JSON字符串**

```json
{"_type":"Received"}
```



- **_type** 

  命令的类型

- **Id** 
  命令的唯一标示，commandId，用于通知下载进度和错误。

- **preDownloadURL** 
  拼接source的id值来下载文件

- **notificationURL** 
  向此地址以http协议Post下载进度： 
  **请求头：** 

  ```
  Content-Type: application/json 
  Card-Id: y10-xxxxx
  ```

- **正文：** 

  ```json
  { 
      "commandId":"abc", 
      "taskItemId": "xxxx", 
      "progress":100 
  } 
  ```

  **若下载途中出错向此地址以http协议Post出错信息，正文数据结构如下： **

  ```json
  { 
      "commandId":"abc", 
      "errorMessage": "xxxx" 
  }
  ```

- **task** 
  熙讯节目任务，结构略。

## 2.上传播放日志

#### 通过AIDL接口executeJsonCommand传递数据。

**命令数据：**

```json
{ 
    "_type": "UploadPlayLogs", 				*//命令类型* 
    "interval": 60, 					   *//整型，单位分钟* 
    "url":"http://xxxxxx"				   *//上传地址* 
    "encoding":"gzip" 					   *//压缩方式* 
}
```

-   **\_type** 
    命令的类型

-   **interval** 
    每interval分钟上传一次播放日志，若interval为0就不定时上传

-   **url** 
    上传地址，使用HTTP协议的POST方法，上传成功会返回状态码200。上传成功后删除数据库的数据

-   **encoding** 
    若值为null或空字符串时，就不使用压缩算法上传日志

**收到命令后，若有错误返回JSON字符串**

```json
{"_type":"Error","errorMessage":"xxxx"}，
```

**_无错误返回JSON字符串_**

```json
{"_type":"Success"}，
```

**并根据interval的值定时向url以http协议发送以下内容(用UTF8编码且以encoding算法压缩正文数据)：**

**请求头：** 

```
Content-Type: application/json 
Content-Encoding: gzip 
Card-Id: y10-xxxxx
```



**正文：** 

```json
[ 
    { 
    "type":"gps", 		   *//节目类型， 枚举值：gps, simple, advanced, usb* 
    "pid":"xxx", 		   *//programId。若是Gps节目，此值为pointId。* 
    "name":"programA", 	 	*//节目名或投放点名* 
    "beginAt":1441518061436,*//时间戳，从格林威治时间1970年01月01日00时00分00秒起的总毫秒数* 
    "duration": 15			*//播放时长，秒* 
    }, 
	**.....//最多1000条** 
]
```



## 3.GPS节目状态切换

#### 通过AIDL接口executeJsonCommand传递数据。

**命令数据：**

```json
{ 
    "_type": "ToggleGpsTask", 		 *//命令类型* 
    "play": true, 					*//true为播放gps任务* 
}
```



## 4.GPS节目状态查询

#### 通过AIDL接口executeJsonCommand传递数据。

**命令数据：**

```json
{ 
	"_type": "GetGpsTaskStatus", 	*//命令类型* 
}
```



返回：

```json
{"_type":"DataCallback","play":true}
```



## 5.查询剩余空间

#### 通过AIDL接口executeJsonCommand传递数据。

**命令数据：**

```json
{ 
	"_type": "GetDiskSpace", *//命令类型* 
}
```



返回：

```json
{"_type":"DataCallback","internalTotal":512, "internalFree":50,"externalTotal":2048, "externalFree":60, },单位MB。
```



## 6.天气预报升级并向下兼容

Weather素材增加属性code,
若code==0，天气的数据读取将使用旧的city属性，访问地址：

```
"http://wthrcdn.etouch.cn/weather_mini?city="+city，
```

若code!=0，访问地址：

```
"http://wthrcdn.etouch.cn/weather_mini?citykey="+code
```



## 7.删除任务与节目数据

#### 通过AIDL接口executeJsonCommand传递数据。

**命令数据：**

```json
{ 
	"_type": "DeleteTask", 		*//命令类型* 
}
```

回复：result:
