[TOC]

# Android开发方案

## 1、方案概述

直接开发安卓apk，这种开放方式最灵活。部分功能需要用到CardSystem的接口（详见xixun_card_setting.zip）。

预备知识：

- 掌握一种能开发安卓的编程语言(JAVA、Kotlin、Scala等）
- 掌握安卓的基础相关知识（Activity，Service，BroadcastReceiver等）

## 2、关于开发apk的版本兼容问题

### 2.1、首先，下载Easyboard软件

访问https://ledok.cn/download.html，找到图中的LedOK软件下载链接

![mage-20200227130556267](../pictures/android-ledok1.png)

### 2.2、查看控制卡安卓版本

打开LedOK

![image-20200227165156967](../pictures/android-ledok2.png)

可以看到详细信息单机即可

![image-20200227165237548](../pictures/android-ledok3.png)

### 2.3、设置apk安卓兼容版本

打开安卓开发工具，在工程的app目录下有一个build.gradle文件，打开该文件

![image-20200227165318393](../pictures/Androidstudio.png)

修改最低兼容版本，下图为版本对照表

| API level | ***\*最初Android版本\**** | ***\*Linux内核版本\**** |
| --------- | ------------------------- | ----------------------- |
| 28        | 9                         |                         |
| 27        | 8.1                       | 4.10                    |
| 26        | 8.0                       | 4.10                    |
| 25        | 7.1                       | 4.4.1                   |
| 24        | 7.0                       | 4.4.1                   |
| 23        | 6.0                       | 3.18.10                 |
| 22        | 5.1                       | 3.16.1                  |
| 21        | 5.0                       | 3.16.1                  |
| 20        | 4.4w                      | 3.10                    |
| 19        | 4.4                       | 3.10                    |
| 18        | 4.3                       | 3.4.0                   |
| 17        | 4.2                       | 3.4.0                   |
| 16        | 4.1                       | 3.0.31                  |
| 15        | 4.0.3                     | 3.0.1                   |
| 14        | 4.0                       | 3.0.1                   |
| 13        | 3.2                       | 2.6.36                  |
| 12        | 3.1                       | 2.6.36                  |
| 11        | 3.0                       | 2.6.36                  |

## 3、如需使用我公司提供的api，需要导入xixun_card_setting.jar文件

本文档同名文件夹下可找到该文件

## 4、将APK安装到控制卡上

打开LedOK,点击终端控制再点击高级设置，输入密码888 先卸载xixunplayer

![image-20200227165528343](../pictures/android-ledok4.png)

点击apk升级，选择apk点击确定，等待提示升级成功即可。