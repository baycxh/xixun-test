﻿

public static String GetCardId(byte[] cBuf) {
        byte[] cMyKey = new byte[]{97, 119, 38, 3, 46, 112, 36, 93, 58, 100, 103, 62, 115, 112, 114, 51, 43, 61, 2, 101, 119};
        if (cBuf.length < 40) {
            return "";
        } else {
            for(int i = 0; i < 20; ++i) {
                cBuf[i] = (byte)(cBuf[i * 2] - cMyKey[i] - i - (cBuf[i * 2 + 1] - 3));
            }

            String strtemp = new String(cBuf);
            if (strtemp.length() >= 13) {
                strtemp = strtemp.substring(0, 13);
            }

            return strtemp;
        }
    }


public static byte[] getByte(String src) {
		File file = new File(src);
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int read;
			while ((read = in.read(buf)) != -1) {
				out.write(buf, 0, read);
			}
			out.close();
			in.close();
			return out.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	 }

final static String CID_PATH = "/data/joey/signed/card.id";
public static String getCardId(){
		byte[] content = getByte(CID_PATH);
		String value = null;
		try{
			value =GetCardId(content);
		}
		catch (Exception e) {
			e.printStackTrace();
		}	
		return value;
	}

	
	 