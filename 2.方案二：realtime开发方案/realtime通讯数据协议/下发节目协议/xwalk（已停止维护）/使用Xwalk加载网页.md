[TOC]

# 使用Xwalk加载网页

## 1、xwalk简介

**需要先在www.m2mled.net上安装xwalk，xwalk是一个支持html5的浏览器**，使用corsswalk内核，相比于顶层网页，xwalk支持h5。

## 2、xwalk协议

### 2.1、启动xwalk

```json
{ 
	"type": "startActivity", 
	"apk": "com.xixun.xy.xwalk" 
}
```

### 2.2、使用xwalk加载网页

**第一次使用xwalk加载网页时，务必先用《启动xwalk》命令打开xwalk才能继续使用xwalk加载网页。若persistent（持久化）为true，之后无需再次调用《启动xwalk》命令，重启控制卡后，系统会自动加载上一次的url；若为false，每次重启，开机后需再次调用《启动xwalk》命令才能继续使用xwalk加载网页。**

**（注意：js里面请不要添加alert这一类需要鼠标操作的代码，否则会卡住。郑重声明：请不要使用在网页中使用video标签，经测试长时间使用video标签播放视频会导致画面卡住！）**

```json
{ 
	"type": "callXwalkFn", 
	"fn": "loadUrl", 
	"arg": { 
		"url":"http://192.168.8.99:808/score.html", 
		"backupUrl":"file:///mnt/sdcard/res/backup.html",*//备用地址,没网时自动加载此地址,（不做持久化时可以省略次url，如果url中显示端口号需要加上80，如http://www.codingke.com:80/v/1926-lesson-228-course）* 
		"persistent": true,  *//持久化，重启会自动加载url* 
		"extra":{ 			*//额外数据(可省略),它的值可以为任意类型，,在网页里使用window.\$extra获取* 
            "a":1, 
            "b": true,
            "c":"abc"
		} 
	} 
}
```

### 2.3、调用xwalk加载的网页里的js方法

```json
{ 
    "type": "callXwalkFn", 
    "fn": "callFnInLoaded", 
	"arg": { 
        "fn": "changeText",						*//此参数为传入fn的数据,可以是任意类型* 
		"arg": { 
            "id":"m2", 
            "content": "/<b>data/</b> in m2"
		} 
	} 
}
```

### 2.4、设置背景

```json
{ 
    "type": "callXwalkFn", 
    "fn": "setBackgroundColor", 
    "arg":"#666666" 
}
```

**注意：当前加载的页面必须已经定义过showHtml方法**

## 3、Xwalk页面内置js方法

```javascript
  1. $card.getScreenWidth() //获取Ledset设置的屏宽
  2. $card.getScreenHeight() //获取Ledset设置的屏高
  3. $card.getDeviceWidth() //获取设备屏宽
  4. $card.getDeviceHeight() //获取设备屏高
  5. $card.getCardId() //获取终端序列号
```

