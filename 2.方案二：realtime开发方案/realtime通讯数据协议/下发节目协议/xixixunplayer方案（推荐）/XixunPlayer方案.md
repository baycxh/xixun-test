[TOC]

# XixunPlayer方案

## 1、方案说明

本方案为熙讯主推播放方案，使用本公司自主开发的播放器"xixunplayer"进行节目的下发与播放，可以实现同步播放效果，并支持单行文本、多行文本、图片、视频、音频、天气预报、环境监测（温度、湿度、风速、风向、pm2.5、pm10）、模拟时钟、数字时钟、倒计时等多种类型的节目。

## 2、协议接口

### 2.1、下发节目

示例代码：

```json
{
  "type":"commandXixunPlayer",
  "_id":"28dc5b76-51c9-11e9-9b24-4ccc6a95f6dc",
  "command":{
    "_type":"PlayXixunTask",
    "id":"28dd5a80-51c9-11e9-9b24-4ccc6a95f6dc",
    "preDownloadURL":"http://8.136.4.199/upload/",
    "notificationURL":"http://192.168.2.105:8083/getJSON",
    "task":{
      "_id":"28dd55c6-51c9-11e9-9b24-4ccc6a95f6dc",
      "name":"图片节目test",
      "items":[
        {
          "_id":"7111bd0f-edba-4db6-8bd4-f5071d79a265",
          "_program":{
            "_id":"28dd2786-51c9-11e9-9b24-4ccc6a95f6dc",
            "totalSize":55066,
            "name":"图片节目test",
            "width":128,
            "height":256,
			 "__v":0,
            "layers":[
              {
				"repeat":false,
                "sources":[
                  {
                    "maxPlayTime":31,
                    "_type":"Image",
                    "md5":"b9bcfa191b1ddb4bdfc6a6057dc534f6",
                    "name":"5s.jpg",
                    "mime":"image/jpeg",
                    "size":55066,
                    "fileExt":".jpg",
                    "id":"5s.jpg",
                    "playTime":0,
                    "timeSpan":10,
                    "left":0,
                    "top":0,
                    "width":128,
                    "height":256,
                    "entryEffect":"None",
                    "exitEffect":"None",
                    "entryEffectTimeSpan":0,
                    "exitEffectTimeSpan":0
                  }
                ]
              }
            ]
          },
          "priority":0,
          "repeatTimes":1,
		  "version":0,
          "schedules":[
            {
              "filterType":"None",		//该参数必须有
              "timeType":"Range",		//该参数必须有
              "startTime":"00:00",
              "endTime":"23:59",
              "dateType":"All"			//该参数必须有
            }
          ]
        }
      ]
    }
  }
}
```

详细参数说明请参考同级文件夹下的“xixunpalyer节目json说明文档“。

### 2.2、清除播放器节目数据和文件(conn10.0.5T或以上版本支持)

JSON数据格式举例：

```json
{ 
    "type": "clearPlayerTask"
} 
```

返回成功格式举例：

```json
{
    "_type": "success",
    "_id": "4ed91a8f-dea2-42fb-951d-c6e3484a97af",
    "timestamp": 1553848772492
}
```

### 2.3、查询播放器当前存储的节目JSON(仅conn10.0.9或以上版本支持)

请求数据格式：

```json
{
    "type": "getProgramTask"
}
```

返回数据格式：

```json
{
    "_id": "7ac3c9f4-07e7-49b1-badb-c680535c6d2e",

    "_type": "success",
"data": "{\"task\":{\"_department\":{\"_company\":\"test\",\"_id\":\"\",\"name\":\"RootDepartment\",\"priority\":0},\"_id\":\"5d08bd360926d29d1ae78ef1\",\"items\":[...],\"name\":\"1212121_Task\"}}"
}
```

### 2.4、查询播放器当前正在播放的节目名(仅conn10.0.9或以上版本支持)

请求数据格式：

```json
{
    "type": "getPlayingProgram"
}
```

应答数据格式：

```json
{
    "_id": "4a5b0a6e-9b56-474a-a9c0-4ca3a2e8b9cb",
 
   "_type": "success",
  
  "name": "1212121"  //节目名
}
```

