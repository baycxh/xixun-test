[TOC]

# 1、realtime开发方案简介

基于熙讯RealtimeSDK使用web技术和http通讯开发（需精通JavaScript），功能有局限，但开发速度较快。

预备知识：

- 掌握html+css
- 精通JavaScript
- 掌握任意一种后端开发语言（C#, JAVA, PHP, nodejs等）

## 1.1、Realtime服务拓扑图

![image](../pictures/tuopu.png)



# 2、Realtime服务环境搭建

## 2.1、概述

- 本解决方案适合有能力开发web的用户本系统提供loadUrl接口可以打开任意网址，提供invokeJs来调用当前页面里的javascript方法（此方法可以传输文本数据）以达到实时显示的目的，用户可以自定义显示界面与逻辑。
- 本方案提供两种语言的realtime服务给客户使用，java版和nodejs版，linux服务器建议使用Java版本。
- java版优势：服务更稳定，支持多线程。
- nodejs版优势：安装部署相对简单。

## 2.2、安装

- Java版：根据操作系统安装对应的jdk并配置环境变量，java1.8以上版本均可，安装步骤可自行百度搜索。
- nodejs版：根据服务器操作系统安装对应的nodejs，下载地址：*https://nodejs.org/download/release/v4.4.7/* ，nodejs下载版本建议在4.4.7或以上。本sdk已提供windows64位和32位的安装程序。

## 2.3、运行realtimeServer（nodejs）

windows系统：按住键盘shift键，右键sdk里的RealtimeServer文件夹，选择“在此处打开命令窗口”，然后输入node bin\www

![img](../pictures/runRealtime.png)

## 2.4、配置realtime服务以及启动（JAVA）

打开 harbour.properties 配置文件, 修改端口号, 数据库地址, 用户名, 密码和连接池. 如果不需要数据库可以不配置数据库.

```xml
server.port : 服务端口

uselog : 是否使用日志文件

callback_url : 设备接入后的回调地址

db.host : 数据库地址+端口
db.user : 数据库用户名
db.password : 数据库密码

db.maxPoolSize : 连接池最大连接数, 默认 30
db.acquireIncrement : 连接池每次增加的连接数, 默认 6
db.maxIdleTime : 每个连接的最大空闲秒数, 默认 1800
```

harbour.properties 示例:

```xml
server.port = 9000

uselog = true

callback_url = http://localhost/mng/aaa/aaa

db.host = localhost:3306
db.user = root
db.password = 123456

db.maxPoolSize = 30
db.acquireIncrement = 6
db.maxIdleTime = 1800
```

windows系统：按住键盘shift键，右键harbour.jar所在的文件夹，选择“在此处打开命令窗口”，然后输入java -jar harbour.jar  命令即可启动程序，如果出现提示: 'java'不是内部或外部命令，则需要安装 java 运行环境。

linux系统：nohup java -jar harbour.jar &

nohup 意思是不挂断运行命令,当账户退出或终端关闭时,程序仍然运行

当用 nohup 命令执行作业时，缺省情况下该作业的所有输出被重定向到nohup.out的文件中，除非另外指定了输出文件。

## 2.4、设置控制卡服务器地址

访问https://ledok.cn/download.html，找到图中的LedOK软件下载链接

![输入图片说明](https://images.gitee.com/uploads/images/2020/1120/093920_e0fb51a2_7915659.png "屏幕截图.png")

下载并安装LedOK软件，安装成功后打开。它会搜索同一个局域网下的所有控制卡。然后点击对应的参数按钮进行配置。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1120/103546_263b1b65_7915659.png "屏幕截图.png")

点击终端控制按钮，再点击高级设置，密码888，进入设置界面（Apk升级与卸载可进行安装和卸载软件等功能）。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1120/103907_70723930_7915659.png "屏幕截图.png")

进入高级设置页面后，输入realtime服务所在的主机ip加端口号默认8081.保存退出。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1120/103930_317491be_7915659.png "屏幕截图.png")

当出现以下内容时即为设置成功

![输入图片说明](https://images.gitee.com/uploads/images/2020/1120/104045_732b0e46_7915659.png "屏幕截图.png")

连接成功后，realtime服务界面会显示如下连接信息

![img](../pictures/connection.png)

## 2.5、修改realtime端口号（nodejs）

如果需要修改realtimeServer的监听端口，进入sdk的realtimeServer目录，用文本编辑器打开config.js文件，把8081改为你需要的端口。
